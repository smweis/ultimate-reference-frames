#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy2 Experiment Builder (v1.85.2),
    on August 04, 2017, at 16:38
If you publish work using this script please cite the PsychoPy publications:
    Peirce, JW (2007) PsychoPy - Psychophysics software in Python.
        Journal of Neuroscience Methods, 162(1-2), 8-13.
    Peirce, JW (2009) Generating stimuli for neuroscience using PsychoPy.
        Frontiers in Neuroinformatics, 2:10. doi: 10.3389/neuro.11.010.2008
"""

from __future__ import absolute_import, division
from psychopy import locale_setup, sound, gui, visual, core, data, event, logging
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)
import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__)).decode(sys.getfilesystemencoding())
os.chdir(_thisDir)

# Store info about the experiment session
expName = 'nav'  # from the Builder filename that created this script
expInfo = {u'Participant ID': u''}
dlg = gui.DlgFromDict(dictionary=expInfo, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data' + os.sep + '%s_%s' % (expInfo['Participant ID'], expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath=u'C:\\Users\\Steve\\Documents\\Dropbox\\Penn Post Doc\\Frisbee_Ref_Frames\\exp\\navwithPractice.psyexp',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.WARNING)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp

# Start Code - component code to be run before the window creation

# Setup the Window
win = visual.Window(
    size=(1600, 900), fullscr=True, screen=0,
    allowGUI=False, allowStencil=False,
    monitor='testMonitor', color='white', colorSpace='rgb',
    blendMode='avg', useFBO=True,
    units='norm')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# Initialize components for Routine "begin_instructions"
begin_instructionsClock = core.Clock()
Welcome = visual.TextStim(win=win, name='Welcome',
    text=u'Welcome to the next part! \n\nThis will take about 10 minutes. \n\n\nPress the space bar to continue.',
    font=u'Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color=u'black', colorSpace='rgb', opacity=1,
    depth=0.0);

# Initialize components for Routine "block_instructions"
block_instructionsClock = core.Clock()
instructions_1 = visual.ImageStim(
    win=win, name='instructions_1',units='norm', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=(1,1.3333),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-1.0)

# Initialize components for Routine "instructions_repeat"
instructions_repeatClock = core.Clock()
text = visual.TextStim(win=win, name='text',
    text=u"If you'd like to repeat the instructions\n    Press 1. \n\nIf you're ready to move on to the practice\n    Press 0. ",
    font=u'Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color=u'black', colorSpace='rgb', opacity=1,
    depth=-1.0);

# Initialize components for Routine "block_instructions_repeat"
block_instructions_repeatClock = core.Clock()
instructions_repeated = visual.ImageStim(
    win=win, name='instructions_repeated',units='norm', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=(1,1.333),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=0.0)

# Initialize components for Routine "practice_begin"
practice_beginClock = core.Clock()
text_5 = visual.TextStim(win=win, name='text_5',
    text=u"Up next, you'll get to practice\na few trials. \n\nYou'll only be able to move\nto the next trial by\nanswering correctly. \n\nPress space to begin.",
    font=u'Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color=u'black', colorSpace='rgb', opacity=1,
    depth=0.0);

# Initialize components for Routine "ISI"
ISIClock = core.Clock()
Fixation = visual.TextStim(win=win, name='Fixation',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1,
    depth=0.0);

# Initialize components for Routine "practice"
practiceClock = core.Clock()
text_3 = visual.TextStim(win=win, name='text_3',
    text='default text',
    font='Arial',
    pos=(0, -.8), height=0.1, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1,
    depth=0.0);
image = visual.ImageStim(
    win=win, name='image',units='height', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=(0.75, 0.75),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=128, interpolate=True, depth=-1.0)

# Initialize components for Routine "practice_feedback"
practice_feedbackClock = core.Clock()
text_6 = visual.TextStim(win=win, name='text_6',
    text='default text',
    font=u'Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color=u'red', colorSpace='rgb', opacity=1,
    depth=0.0);

# Initialize components for Routine "end_practice"
end_practiceClock = core.Clock()
text_4 = visual.TextStim(win=win, name='text_4',
    text=u'That concludes the practice. \n\nThe actual task consists of 64 trials. \nIt will take between 5-10 minutes.\nIt is critical that you\nDO NOT STOP IN THE MIDDLE. \n\nRemember to respond as \nQUICKLY AND ACCURATELY\nas you can.\n\nThe actual study will begin\nas soon as\nyou press SPACE. \n',
    font=u'Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color=u'black', colorSpace='rgb', opacity=1,
    depth=0.0);

# Initialize components for Routine "ISI"
ISIClock = core.Clock()
Fixation = visual.TextStim(win=win, name='Fixation',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1,
    depth=0.0);

# Initialize components for Routine "trial"
trialClock = core.Clock()
Trial_image = visual.ImageStim(
    win=win, name='Trial_image',units='height', 
    image='sin', mask=None,
    ori=0, pos=(0, 0), size=(.75,.75),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=32, interpolate=True, depth=-1.0)
text_2 = visual.TextStim(win=win, name='text_2',
    text='default text',
    font='Arial',
    pos=(0, -.8), height=0.1, wrapWidth=None, ori=0, 
    color='black', colorSpace='rgb', opacity=1,
    depth=-2.0);

# Initialize components for Routine "block_finished"
block_finishedClock = core.Clock()
Block_Finished = visual.TextStim(win=win, name='Block_Finished',
    text=u"That concludes this section. \nFeel free to take a break. \n\nFor the next part you'll need to \nenter the code below back on the \nfirst website. \n\nWrite it down exactly,\nand then press SPACE, which \nwill close this window. ",
    font=u'Arial',
    units='norm', pos=(0, .3), height=0.1, wrapWidth=None, ori=0, 
    color=u'black', colorSpace='rgb', opacity=1,
    depth=0.0);
code = visual.TextStim(win=win, name='code',
    text=u'\n\nnavstudy4017',
    font=u'Arial',
    pos=(0, -.5), height=0.1, wrapWidth=None, ori=0, 
    color=u'red', colorSpace='rgb', opacity=1,
    depth=-2.0);

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "begin_instructions"-------
t = 0
begin_instructionsClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
Continue = event.BuilderKeyResponse()
# keep track of which components have finished
begin_instructionsComponents = [Welcome, Continue]
for thisComponent in begin_instructionsComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "begin_instructions"-------
while continueRoutine:
    # get current time
    t = begin_instructionsClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *Welcome* updates
    if t >= 0.0 and Welcome.status == NOT_STARTED:
        # keep track of start time/frame for later
        Welcome.tStart = t
        Welcome.frameNStart = frameN  # exact frame index
        Welcome.setAutoDraw(True)
    
    # *Continue* updates
    if t >= 1.0 and Continue.status == NOT_STARTED:
        # keep track of start time/frame for later
        Continue.tStart = t
        Continue.frameNStart = frameN  # exact frame index
        Continue.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(Continue.clock.reset)  # t=0 on next screen flip
        event.clearEvents(eventType='keyboard')
    if Continue.status == STARTED:
        theseKeys = event.getKeys(keyList=['y', 'n', 'left', 'right', 'space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            Continue.keys = theseKeys[-1]  # just the last key pressed
            Continue.rt = Continue.clock.getTime()
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in begin_instructionsComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "begin_instructions"-------
for thisComponent in begin_instructionsComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if Continue.keys in ['', [], None]:  # No response was made
    Continue.keys=None
thisExp.addData('Continue.keys',Continue.keys)
if Continue.keys != None:  # we had a response
    thisExp.addData('Continue.rt', Continue.rt)
thisExp.nextEntry()
# the Routine "begin_instructions" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
instructions_1_loop = data.TrialHandler(nReps=1, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions(u'navInstructions.csv'),
    seed=None, name='instructions_1_loop')
thisExp.addLoop(instructions_1_loop)  # add the loop to the experiment
thisInstructions_1_loop = instructions_1_loop.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisInstructions_1_loop.rgb)
if thisInstructions_1_loop != None:
    for paramName in thisInstructions_1_loop.keys():
        exec(paramName + '= thisInstructions_1_loop.' + paramName)

for thisInstructions_1_loop in instructions_1_loop:
    currentLoop = instructions_1_loop
    # abbreviate parameter names if possible (e.g. rgb = thisInstructions_1_loop.rgb)
    if thisInstructions_1_loop != None:
        for paramName in thisInstructions_1_loop.keys():
            exec(paramName + '= thisInstructions_1_loop.' + paramName)
    
    # ------Prepare to start Routine "block_instructions"-------
    t = 0
    block_instructionsClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    instructions_response = event.BuilderKeyResponse()
    instructions_1.setImage("design/nav_instructions/" + Image)
    # keep track of which components have finished
    block_instructionsComponents = [instructions_response, instructions_1]
    for thisComponent in block_instructionsComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "block_instructions"-------
    while continueRoutine:
        # get current time
        t = block_instructionsClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *instructions_response* updates
        if t >= 1.0 and instructions_response.status == NOT_STARTED:
            # keep track of start time/frame for later
            instructions_response.tStart = t
            instructions_response.frameNStart = frameN  # exact frame index
            instructions_response.status = STARTED
            # keyboard checking is just starting
            win.callOnFlip(instructions_response.clock.reset)  # t=0 on next screen flip
            event.clearEvents(eventType='keyboard')
        if instructions_response.status == STARTED:
            theseKeys = event.getKeys(keyList=['y', 'n', 'left', 'right', 'space'])
            
            # check for quit:
            if "escape" in theseKeys:
                endExpNow = True
            if len(theseKeys) > 0:  # at least one key was pressed
                instructions_response.keys = theseKeys[-1]  # just the last key pressed
                instructions_response.rt = instructions_response.clock.getTime()
                # a response ends the routine
                continueRoutine = False
        
        # *instructions_1* updates
        if t >= 0.0 and instructions_1.status == NOT_STARTED:
            # keep track of start time/frame for later
            instructions_1.tStart = t
            instructions_1.frameNStart = frameN  # exact frame index
            instructions_1.setAutoDraw(True)
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in block_instructionsComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "block_instructions"-------
    for thisComponent in block_instructionsComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # check responses
    if instructions_response.keys in ['', [], None]:  # No response was made
        instructions_response.keys=None
    instructions_1_loop.addData('instructions_response.keys',instructions_response.keys)
    if instructions_response.keys != None:  # we had a response
        instructions_1_loop.addData('instructions_response.rt', instructions_response.rt)
    # the Routine "block_instructions" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed 1 repeats of 'instructions_1_loop'

# get names of stimulus parameters
if instructions_1_loop.trialList in ([], [None], None):
    params = []
else:
    params = instructions_1_loop.trialList[0].keys()
# save data for this loop
instructions_1_loop.saveAsExcel(filename + '.xlsx', sheetName='instructions_1_loop',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "instructions_repeat"-------
t = 0
instructions_repeatClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
repeat_response = event.BuilderKeyResponse()
# keep track of which components have finished
instructions_repeatComponents = [repeat_response, text]
for thisComponent in instructions_repeatComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "instructions_repeat"-------
while continueRoutine:
    # get current time
    t = instructions_repeatClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *repeat_response* updates
    if t >= 1.0 and repeat_response.status == NOT_STARTED:
        # keep track of start time/frame for later
        repeat_response.tStart = t
        repeat_response.frameNStart = frameN  # exact frame index
        repeat_response.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(repeat_response.clock.reset)  # t=0 on next screen flip
        event.clearEvents(eventType='keyboard')
    if repeat_response.status == STARTED:
        theseKeys = event.getKeys(keyList=['0', '1'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            repeat_response.keys = theseKeys[-1]  # just the last key pressed
            repeat_response.rt = repeat_response.clock.getTime()
            # a response ends the routine
            continueRoutine = False
    
    # *text* updates
    if t >= 0.0 and text.status == NOT_STARTED:
        # keep track of start time/frame for later
        text.tStart = t
        text.frameNStart = frameN  # exact frame index
        text.setAutoDraw(True)
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in instructions_repeatComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "instructions_repeat"-------
for thisComponent in instructions_repeatComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if repeat_response.keys in ['', [], None]:  # No response was made
    repeat_response.keys=None
thisExp.addData('repeat_response.keys',repeat_response.keys)
if repeat_response.keys != None:  # we had a response
    thisExp.addData('repeat_response.rt', repeat_response.rt)
thisExp.nextEntry()
# the Routine "instructions_repeat" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
instructions_2_repeat = data.TrialHandler(nReps=repeat_response.keys, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions(u'navInstructions.csv'),
    seed=None, name='instructions_2_repeat')
thisExp.addLoop(instructions_2_repeat)  # add the loop to the experiment
thisInstructions_2_repeat = instructions_2_repeat.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisInstructions_2_repeat.rgb)
if thisInstructions_2_repeat != None:
    for paramName in thisInstructions_2_repeat.keys():
        exec(paramName + '= thisInstructions_2_repeat.' + paramName)

for thisInstructions_2_repeat in instructions_2_repeat:
    currentLoop = instructions_2_repeat
    # abbreviate parameter names if possible (e.g. rgb = thisInstructions_2_repeat.rgb)
    if thisInstructions_2_repeat != None:
        for paramName in thisInstructions_2_repeat.keys():
            exec(paramName + '= thisInstructions_2_repeat.' + paramName)
    
    # ------Prepare to start Routine "block_instructions_repeat"-------
    t = 0
    block_instructions_repeatClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    instructions_repeated.setImage("design/nav_instructions/" + Image)
    key_resp_3 = event.BuilderKeyResponse()
    # keep track of which components have finished
    block_instructions_repeatComponents = [instructions_repeated, key_resp_3]
    for thisComponent in block_instructions_repeatComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "block_instructions_repeat"-------
    while continueRoutine:
        # get current time
        t = block_instructions_repeatClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *instructions_repeated* updates
        if t >= 0.0 and instructions_repeated.status == NOT_STARTED:
            # keep track of start time/frame for later
            instructions_repeated.tStart = t
            instructions_repeated.frameNStart = frameN  # exact frame index
            instructions_repeated.setAutoDraw(True)
        
        # *key_resp_3* updates
        if t >= 1.0 and key_resp_3.status == NOT_STARTED:
            # keep track of start time/frame for later
            key_resp_3.tStart = t
            key_resp_3.frameNStart = frameN  # exact frame index
            key_resp_3.status = STARTED
            # keyboard checking is just starting
            win.callOnFlip(key_resp_3.clock.reset)  # t=0 on next screen flip
            event.clearEvents(eventType='keyboard')
        if key_resp_3.status == STARTED:
            theseKeys = event.getKeys(keyList=['y', 'n', 'left', 'right', 'space'])
            
            # check for quit:
            if "escape" in theseKeys:
                endExpNow = True
            if len(theseKeys) > 0:  # at least one key was pressed
                key_resp_3.keys = theseKeys[-1]  # just the last key pressed
                key_resp_3.rt = key_resp_3.clock.getTime()
                # a response ends the routine
                continueRoutine = False
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in block_instructions_repeatComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "block_instructions_repeat"-------
    for thisComponent in block_instructions_repeatComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # check responses
    if key_resp_3.keys in ['', [], None]:  # No response was made
        key_resp_3.keys=None
    instructions_2_repeat.addData('key_resp_3.keys',key_resp_3.keys)
    if key_resp_3.keys != None:  # we had a response
        instructions_2_repeat.addData('key_resp_3.rt', key_resp_3.rt)
    # the Routine "block_instructions_repeat" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
# completed repeat_response.keys repeats of 'instructions_2_repeat'


# ------Prepare to start Routine "practice_begin"-------
t = 0
practice_beginClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_5 = event.BuilderKeyResponse()
# keep track of which components have finished
practice_beginComponents = [text_5, key_resp_5]
for thisComponent in practice_beginComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "practice_begin"-------
while continueRoutine:
    # get current time
    t = practice_beginClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_5* updates
    if t >= 0.0 and text_5.status == NOT_STARTED:
        # keep track of start time/frame for later
        text_5.tStart = t
        text_5.frameNStart = frameN  # exact frame index
        text_5.setAutoDraw(True)
    
    # *key_resp_5* updates
    if t >= 1.0 and key_resp_5.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_5.tStart = t
        key_resp_5.frameNStart = frameN  # exact frame index
        key_resp_5.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_5.clock.reset)  # t=0 on next screen flip
        event.clearEvents(eventType='keyboard')
    if key_resp_5.status == STARTED:
        theseKeys = event.getKeys(keyList=['space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            key_resp_5.keys = theseKeys[-1]  # just the last key pressed
            key_resp_5.rt = key_resp_5.clock.getTime()
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in practice_beginComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "practice_begin"-------
for thisComponent in practice_beginComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_5.keys in ['', [], None]:  # No response was made
    key_resp_5.keys=None
thisExp.addData('key_resp_5.keys',key_resp_5.keys)
if key_resp_5.keys != None:  # we had a response
    thisExp.addData('key_resp_5.rt', key_resp_5.rt)
thisExp.nextEntry()
# the Routine "practice_begin" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
trials_2 = data.TrialHandler(nReps=1, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions(u'navPractice.csv'),
    seed=None, name='trials_2')
thisExp.addLoop(trials_2)  # add the loop to the experiment
thisTrial_2 = trials_2.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTrial_2.rgb)
if thisTrial_2 != None:
    for paramName in thisTrial_2.keys():
        exec(paramName + '= thisTrial_2.' + paramName)

for thisTrial_2 in trials_2:
    currentLoop = trials_2
    # abbreviate parameter names if possible (e.g. rgb = thisTrial_2.rgb)
    if thisTrial_2 != None:
        for paramName in thisTrial_2.keys():
            exec(paramName + '= thisTrial_2.' + paramName)
    
    # ------Prepare to start Routine "ISI"-------
    t = 0
    ISIClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    # keep track of which components have finished
    ISIComponents = [Fixation]
    for thisComponent in ISIComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "ISI"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = ISIClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *Fixation* updates
        if t >= 0.0 and Fixation.status == NOT_STARTED:
            # keep track of start time/frame for later
            Fixation.tStart = t
            Fixation.frameNStart = frameN  # exact frame index
            Fixation.setAutoDraw(True)
        frameRemains = 0.0 + 1.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if Fixation.status == STARTED and t >= frameRemains:
            Fixation.setAutoDraw(False)
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ISIComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "ISI"-------
    for thisComponent in ISIComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    
    # ------Prepare to start Routine "practice"-------
    t = 0
    practiceClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    text_3.setText(prompt)
    image.setImage("design/" + stim)
    key_resp_2 = event.BuilderKeyResponse()
    # keep track of which components have finished
    practiceComponents = [text_3, image, key_resp_2]
    for thisComponent in practiceComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "practice"-------
    while continueRoutine:
        # get current time
        t = practiceClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_3* updates
        if t >= 1.0 and text_3.status == NOT_STARTED:
            # keep track of start time/frame for later
            text_3.tStart = t
            text_3.frameNStart = frameN  # exact frame index
            text_3.setAutoDraw(True)
        
        # *image* updates
        if t >= 0.0 and image.status == NOT_STARTED:
            # keep track of start time/frame for later
            image.tStart = t
            image.frameNStart = frameN  # exact frame index
            image.setAutoDraw(True)
        
        # *key_resp_2* updates
        if t >= 1.0 and key_resp_2.status == NOT_STARTED:
            # keep track of start time/frame for later
            key_resp_2.tStart = t
            key_resp_2.frameNStart = frameN  # exact frame index
            key_resp_2.status = STARTED
            # keyboard checking is just starting
            win.callOnFlip(key_resp_2.clock.reset)  # t=0 on next screen flip
            event.clearEvents(eventType='keyboard')
        if key_resp_2.status == STARTED:
            theseKeys = event.getKeys(keyList=['left', 'right'])
            
            # check for quit:
            if "escape" in theseKeys:
                endExpNow = True
            if len(theseKeys) > 0:  # at least one key was pressed
                key_resp_2.keys = theseKeys[-1]  # just the last key pressed
                key_resp_2.rt = key_resp_2.clock.getTime()
                # a response ends the routine
                continueRoutine = False
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in practiceComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "practice"-------
    for thisComponent in practiceComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # check responses
    if key_resp_2.keys in ['', [], None]:  # No response was made
        key_resp_2.keys=None
    trials_2.addData('key_resp_2.keys',key_resp_2.keys)
    if key_resp_2.keys != None:  # we had a response
        trials_2.addData('key_resp_2.rt', key_resp_2.rt)
    # the Routine "practice" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # ------Prepare to start Routine "practice_feedback"-------
    t = 0
    practice_feedbackClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(3.000000)
    # update component parameters for each repeat
    text_6.setText(answer + """ arrow key

was the correct answer.

The next trial is coming up shortly."""

)
    # keep track of which components have finished
    practice_feedbackComponents = [text_6]
    for thisComponent in practice_feedbackComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "practice_feedback"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = practice_feedbackClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_6* updates
        if t >= 0.0 and text_6.status == NOT_STARTED:
            # keep track of start time/frame for later
            text_6.tStart = t
            text_6.frameNStart = frameN  # exact frame index
            text_6.setAutoDraw(True)
        frameRemains = 0.0 + 3.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if text_6.status == STARTED and t >= frameRemains:
            text_6.setAutoDraw(False)
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in practice_feedbackComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "practice_feedback"-------
    for thisComponent in practice_feedbackComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.nextEntry()
    
# completed 1 repeats of 'trials_2'

# get names of stimulus parameters
if trials_2.trialList in ([], [None], None):
    params = []
else:
    params = trials_2.trialList[0].keys()
# save data for this loop
trials_2.saveAsExcel(filename + '.xlsx', sheetName='trials_2',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "end_practice"-------
t = 0
end_practiceClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_4 = event.BuilderKeyResponse()
# keep track of which components have finished
end_practiceComponents = [text_4, key_resp_4]
for thisComponent in end_practiceComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "end_practice"-------
while continueRoutine:
    # get current time
    t = end_practiceClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_4* updates
    if t >= 0.0 and text_4.status == NOT_STARTED:
        # keep track of start time/frame for later
        text_4.tStart = t
        text_4.frameNStart = frameN  # exact frame index
        text_4.setAutoDraw(True)
    
    # *key_resp_4* updates
    if t >= 2.0 and key_resp_4.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_4.tStart = t
        key_resp_4.frameNStart = frameN  # exact frame index
        key_resp_4.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_4.clock.reset)  # t=0 on next screen flip
        event.clearEvents(eventType='keyboard')
    if key_resp_4.status == STARTED:
        theseKeys = event.getKeys(keyList=['space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            key_resp_4.keys = theseKeys[-1]  # just the last key pressed
            key_resp_4.rt = key_resp_4.clock.getTime()
            # a response ends the routine
            continueRoutine = False
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in end_practiceComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "end_practice"-------
for thisComponent in end_practiceComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_4.keys in ['', [], None]:  # No response was made
    key_resp_4.keys=None
thisExp.addData('key_resp_4.keys',key_resp_4.keys)
if key_resp_4.keys != None:  # we had a response
    thisExp.addData('key_resp_4.rt', key_resp_4.rt)
thisExp.nextEntry()
# the Routine "end_practice" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
trials = data.TrialHandler(nReps=4.0, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('navTrials.csv'),
    seed=None, name='trials')
thisExp.addLoop(trials)  # add the loop to the experiment
thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
if thisTrial != None:
    for paramName in thisTrial.keys():
        exec(paramName + '= thisTrial.' + paramName)

for thisTrial in trials:
    currentLoop = trials
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial.keys():
            exec(paramName + '= thisTrial.' + paramName)
    
    # ------Prepare to start Routine "ISI"-------
    t = 0
    ISIClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    # keep track of which components have finished
    ISIComponents = [Fixation]
    for thisComponent in ISIComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "ISI"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = ISIClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *Fixation* updates
        if t >= 0.0 and Fixation.status == NOT_STARTED:
            # keep track of start time/frame for later
            Fixation.tStart = t
            Fixation.frameNStart = frameN  # exact frame index
            Fixation.setAutoDraw(True)
        frameRemains = 0.0 + 1.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if Fixation.status == STARTED and t >= frameRemains:
            Fixation.setAutoDraw(False)
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ISIComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "ISI"-------
    for thisComponent in ISIComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    
    # ------Prepare to start Routine "trial"-------
    t = 0
    trialClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    Trials_response = event.BuilderKeyResponse()
    Trial_image.setImage("design/" + stim)
    text_2.setText(prompt)
    # keep track of which components have finished
    trialComponents = [Trials_response, Trial_image, text_2]
    for thisComponent in trialComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "trial"-------
    while continueRoutine:
        # get current time
        t = trialClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *Trials_response* updates
        if t >= 1.0 and Trials_response.status == NOT_STARTED:
            # keep track of start time/frame for later
            Trials_response.tStart = t
            Trials_response.frameNStart = frameN  # exact frame index
            Trials_response.status = STARTED
            # keyboard checking is just starting
            win.callOnFlip(Trials_response.clock.reset)  # t=0 on next screen flip
            event.clearEvents(eventType='keyboard')
        if Trials_response.status == STARTED:
            theseKeys = event.getKeys(keyList=['left', 'right'])
            
            # check for quit:
            if "escape" in theseKeys:
                endExpNow = True
            if len(theseKeys) > 0:  # at least one key was pressed
                Trials_response.keys = theseKeys[-1]  # just the last key pressed
                Trials_response.rt = Trials_response.clock.getTime()
                # was this 'correct'?
                if (Trials_response.keys == str(answer)) or (Trials_response.keys == answer):
                    Trials_response.corr = 1
                else:
                    Trials_response.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # *Trial_image* updates
        if t >= 0.0 and Trial_image.status == NOT_STARTED:
            # keep track of start time/frame for later
            Trial_image.tStart = t
            Trial_image.frameNStart = frameN  # exact frame index
            Trial_image.setAutoDraw(True)
        
        # *text_2* updates
        if t >= 1.0 and text_2.status == NOT_STARTED:
            # keep track of start time/frame for later
            text_2.tStart = t
            text_2.frameNStart = frameN  # exact frame index
            text_2.setAutoDraw(True)
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "trial"-------
    for thisComponent in trialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # check responses
    if Trials_response.keys in ['', [], None]:  # No response was made
        Trials_response.keys=None
        # was no response the correct answer?!
        if str(answer).lower() == 'none':
           Trials_response.corr = 1  # correct non-response
        else:
           Trials_response.corr = 0  # failed to respond (incorrectly)
    # store data for trials (TrialHandler)
    trials.addData('Trials_response.keys',Trials_response.keys)
    trials.addData('Trials_response.corr', Trials_response.corr)
    if Trials_response.keys != None:  # we had a response
        trials.addData('Trials_response.rt', Trials_response.rt)
    # the Routine "trial" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed 4.0 repeats of 'trials'

# get names of stimulus parameters
if trials.trialList in ([], [None], None):
    params = []
else:
    params = trials.trialList[0].keys()
# save data for this loop
trials.saveAsExcel(filename + '.xlsx', sheetName='trials',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "block_finished"-------
t = 0
block_finishedClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
Block_Continue = event.BuilderKeyResponse()
# keep track of which components have finished
block_finishedComponents = [Block_Finished, Block_Continue, code]
for thisComponent in block_finishedComponents:
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "block_finished"-------
while continueRoutine:
    # get current time
    t = block_finishedClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *Block_Finished* updates
    if t >= 0.0 and Block_Finished.status == NOT_STARTED:
        # keep track of start time/frame for later
        Block_Finished.tStart = t
        Block_Finished.frameNStart = frameN  # exact frame index
        Block_Finished.setAutoDraw(True)
    
    # *Block_Continue* updates
    if t >= 2.0 and Block_Continue.status == NOT_STARTED:
        # keep track of start time/frame for later
        Block_Continue.tStart = t
        Block_Continue.frameNStart = frameN  # exact frame index
        Block_Continue.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(Block_Continue.clock.reset)  # t=0 on next screen flip
        event.clearEvents(eventType='keyboard')
    if Block_Continue.status == STARTED:
        theseKeys = event.getKeys(keyList=['space'])
        
        # check for quit:
        if "escape" in theseKeys:
            endExpNow = True
        if len(theseKeys) > 0:  # at least one key was pressed
            Block_Continue.keys = theseKeys[-1]  # just the last key pressed
            Block_Continue.rt = Block_Continue.clock.getTime()
            # a response ends the routine
            continueRoutine = False
    
    # *code* updates
    if t >= 0.0 and code.status == NOT_STARTED:
        # keep track of start time/frame for later
        code.tStart = t
        code.frameNStart = frameN  # exact frame index
        code.setAutoDraw(True)
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in block_finishedComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # check for quit (the Esc key)
    if endExpNow or event.getKeys(keyList=["escape"]):
        core.quit()
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "block_finished"-------
for thisComponent in block_finishedComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if Block_Continue.keys in ['', [], None]:  # No response was made
    Block_Continue.keys=None
thisExp.addData('Block_Continue.keys',Block_Continue.keys)
if Block_Continue.keys != None:  # we had a response
    thisExp.addData('Block_Continue.rt', Block_Continue.rt)
thisExp.nextEntry()
# the Routine "block_finished" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()
# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
